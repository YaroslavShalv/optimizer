<?php

namespace App\Exceptions;

/**
 * Class PermissionExceptions
 * @package App\Exceptions
 */
class PermissionExceptions extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'Permission denied';

    /**
     * @var string
     */
    protected $code = '403';
}
