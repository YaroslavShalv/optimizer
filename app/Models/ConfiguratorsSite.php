<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConfiguratorsSite
 * @package App
 * @property int $id
 * @property int $site_id
 * @property int $configurator_id
 * @property string $value
 */
class ConfiguratorsSite extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'site_id',
        'configurator_id',
        'value',
    ];
}
