<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Monolog\Logger;

/**
 * Class Log
 * @package App\Models
 * @property int $id
 * @property string $message
 * @property string $channel
 * @property int $level
 * @property string $level_name
 * @property int $unix_time
 * @property string $datetime
 * @property string $context
 * @property string $extra
 * @property string $created_at
 * @property string $updated_at
 */
class Log extends Model
{
    /**
     * @var string
     */
    protected $table = 'log';

    /**
     * This is a static variable and not a constant to serve as an extension point for custom levels
     *
     * @var string[] $levels Logging levels with the levels as key
     */
    public static $levels = [
        Logger::DEBUG => 'DEBUG',
        Logger::INFO => 'INFO',
        Logger::NOTICE => 'NOTICE',
        Logger::WARNING => 'WARNING',
        Logger::ERROR => 'ERROR',
        Logger::CRITICAL => 'CRITICAL',
        Logger::ALERT => 'ALERT',
        Logger::EMERGENCY => 'EMERGENCY',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
        'channel',
        'level',
        'level_name',
        'unix_time',
        'datetime',
        'context',
        'extra',
    ];
}
