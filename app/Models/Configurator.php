<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Configurator
 * @package App\Models
 * @property int $id
 * @property int $type
 * @property int $status
 * @property string $key
 * @property string $defaultValue
 * @property string $name
 */
class Configurator extends Model
{
    const TYPE_CHECKBOX = 1;
    const TYPE_RADIO = 2;
    const TYPE_TEXT = 3;
    const TYPE_INT = 4;
    const TYPE_FLOAT = 5;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'status',
        'key',
        'defaultValue',
        'name',
    ];

    /**
     * @return array
     */
    public static function statusArray() : array
    {
        return [0 => 'Off', 1 => 'On'];
    }

    /**
     * @param int $type
     * @return string
     */
    public static function getNameByType($type = null) : string
    {
        if ($type === null) {
            return  '';
        }

        $types = self::getTypes();
        return isset($types[$type]) ? $types[$type] : '';
    }

    /**
     * @return array
     */
    public static function getTypes() : array
    {
        return [
            self::TYPE_CHECKBOX => 'Checkobox',
            self::TYPE_RADIO => 'Radio',
            self::TYPE_TEXT => 'Text',
            self::TYPE_INT => 'Integer',
            self::TYPE_FLOAT => 'Float',
        ];
    }


    /**
     * Scope a query to only include active users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
