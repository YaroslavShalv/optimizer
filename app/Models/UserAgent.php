<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserAgent
 * @package App
 * @property string $name
 * @property string $regexp
 */
class UserAgent extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'regexp',
    ];
}
