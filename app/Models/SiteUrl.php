<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SiteUrl
 * @package App\Models
 */
class SiteUrl extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'url',
        'site_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function siteUrlsLog()
    {
        return $this->hasMany(SiteUrlLog::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * @return string[]
     */
    public static function statusArray()
    {
        return [
            self::STATUS_PENDING => 'Pending',
            self::STATUS_SUCCESS => 'Success',
            self::STATUS_ERROR => 'Error',
        ];
    }
}
