<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Site
 * @package App\Models
 * @property string $url
 * @property string $public_key
 * @property string $private_key
 */
class Site extends Model
{
    const STATUS_PAYED = 1;
    const STATUS_NOT_PAYED = 2;
    const STATUS_PENDING = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'url',
        'public_key',
        'private_key',
        'user_id',
        'pay_status',
        'requisites',
    ];

    /**
     * @return string[]
     */
    public static function statusArray()
    {
        return [0 => 'Off', 1 => 'On'];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function siteUrls()
    {
        return $this->hasMany(SiteUrl::class);
    }

    public function admin()
    {
        return $this->hasOne(Admin::class, 'id', 'user_id');
    }

    /**
     * @return string[]
     */
    public static function payStatusArray()
    {
        return [
            self::STATUS_PENDING => 'Waiting payment',
            self::STATUS_PAYED => 'Payed',
            self::STATUS_NOT_PAYED => 'Not payed',
        ];
    }
}
