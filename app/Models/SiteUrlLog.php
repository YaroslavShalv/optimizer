<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SiteUrlLog
 * @package App\Models
 */
class SiteUrlLog extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parse_log',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function siteUrl()
    {
        return $this->belongsTo(SiteUrl::class);
    }

    /**
     * @return string[]
     */
    public static function statusArray()
    {
        return [
            self::STATUS_PENDING => 'Pending',
            self::STATUS_SUCCESS => 'Success',
            self::STATUS_ERROR => 'Error',
        ];
    }
}
