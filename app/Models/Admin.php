<?php


namespace App\Models;


use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Role;
use Illuminate\Support\Facades\DB;

/**
 * Class Admin
 * @package App\Models
 *
 * @property Role[] $roles
 */
class Admin extends Administrator
{
    const ROLE_ADMIN = 'administrator';
    const ROLE_CLIENT = 'Client';

    /**
     * @return bool
     */
    public function isAdmin()
    {
        foreach ($this->roles as $role) {
            if ($role->slug === self::ROLE_ADMIN) {
                return true;
            }
        }

        return false;
    }

    public static function getCurrentUser()
    {
        return \Encore\Admin\Facades\Admin::guard()->user();
    }

    /**
     * @return array
     */
    public static function getUserList()
    {
        $users = DB::table('admin_users')->newQuery()
            ->from('admin_users')
            ->leftJoin('admin_role_users as aru', 'aru.user_id', '=', 'admin_users.id')
            ->leftJoin('admin_roles as ar', 'ar.id', '=', 'aru.role_id')
            ->where(['ar.slug' => self::ROLE_CLIENT])
            ->get([
                'admin_users.id',
                'admin_users.name',
                'admin_users.username',
            ]);

        $usersById = [];
        foreach ($users as $user) {
            $usersById[$user->id] = $user->name . ' ' . $user->username;
        }

        return $usersById;
    }
}
