<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\UserAgent;
use App\Services\ParseSiteService;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class OptimizerController
 * @package App\Http\Controllers\Api
 */
class OptimizerController extends Controller
{
    /**
     * @param Request $request
     * @throws HttpException
     */
    public function request(Request $request)
    {
        $params = json_decode($request->getContent(), true);
        if (!isset($params['link']) || strlen($params['link']) < 5) {
            throw new HttpException('404', 'Site not found');
        }

        $service = new ParseSiteService($params['link'], $params['publicKey']);
        echo $service->optimizeSite();
    }

    /**
     * @param Request $request
     * @param UserAgent $agent
     * @return string
     */
    public function userAgent(Request $request, UserAgent $agent)
    {
        return $agent::query()->where(['status' => 1])->get(['regexp'])->toJson();
    }
}
