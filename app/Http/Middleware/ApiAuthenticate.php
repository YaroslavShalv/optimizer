<?php

namespace App\Http\Middleware;

use App\Models\Site;
use App\Services\Security\SecurityCheckService;
use Illuminate\Http\Request;
use App\Services\Security\Exceptions\SecurityException;
use Illuminate\Support\Facades\Log;

class ApiAuthenticate
{

    /**
     * @param Request $request
     * @param \Closure $next
     * @param null $guard
     * @return
     * @throws SecurityException
     */
    public function handle(Request $request, \Closure $next, $guard = null)
    {
        if ($request->route()->getActionName() == 'App\Http\Controllers\Api\OptimizerController@userAgent') {
            return $next($request);
        }

        $service = new SecurityCheckService(
            $request,
            new Site(),
            'public_key',
            'private_key'
        );
        try {
            $service->validateSignature();
        } catch (\Exception $exception) {
            Log::error('Security error', [
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile(),
                'code' => $exception->getCode(),
                'previous' => $exception->getPrevious(),
                'request' => $_REQUEST,
                'ip' => $request->getClientIps(),
                'content' => $request->getContent(),
                'authHeader' => $request->header('Optimizer-Auth'),
                'headers' => $request->headers->all(),
            ]);
            throw new SecurityException();
        }

        Log::debug('Success request', ['request' => $request->getContent()]);

        return $next($request);
    }
}
