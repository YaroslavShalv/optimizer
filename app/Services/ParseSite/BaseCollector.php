<?php

namespace App\Services\ParseSite;

/**
 * Class Collector
 * @package App\ParseSite\Services
 */
abstract class BaseCollector
{
    /**
     * @var string
     */
    protected $regEx;

    /**
     * @var string
     */
    protected $siteUrl;

    /**
     * @var UrlNormalizer
     */
    protected $urlNormalizer;

    /**
     * @var
     */
    protected $siteData;

    /**
     * BaseCollector constructor.
     * @param array $params
     */
    public function __construct($params = array())
    {

    }

    /**
     * @param $baseUrl
     * @param $siteData
     */
    public function setSiteData($baseUrl, $siteData)
    {
        $this->siteUrl = $baseUrl;
        $this->siteData = $siteData;
        $this->urlNormalizer = new UrlNormalizer($baseUrl);
    }

    /**
     * @return mixed
     */
    protected function getSiteData()
    {
        return $this->siteData;
    }

    /**
     * @return array
     */
    protected function findElements()
    {
        preg_match_all($this->regEx, $this->getSiteData(), $tags);
        return $tags;
    }

    /**
     * @param string $url
     * @return Downloader
     */
    protected function downloadData($url)
    {
        $downloader = new Downloader();
        return $downloader->download($url);
    }

    /**
     * @return mixed
     */
    abstract public function parse();
}

