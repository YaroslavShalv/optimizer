<?php

namespace App\Services\ParseSite\Collectors;

use App\Services\ParseSite\BaseCollector;
use MatthiasMullie\Minify\CSS;

/**
 * Class CSSCollector
 * @package App\Services\ParseSite\Collectors
 */
class CSSCollector extends BaseCollector
{
    /**
     * @var string
     */
    protected $regEx = '/<style[\s\S]*?>[\s\S]*?<([\s]+)?\/([\s]+)?style>|<link[^>]+?rel=[\'"]stylesheet[\'"][\S\s^>]*?>/';

    /**
     * @var bool
     */
    private $cutFonts = false;

    /**
     * @var array
     */
    private $toRemove = [];

    /**
     * CSSCollector constructor.
     * @param array $params
     */
    public function __construct($params = array())
    {
        $this->cutFonts = isset($params['cutFonts']) ? (bool) $params['cutFonts'] : false;
    }

    /**
     * @inheritDoc
     */
    public function parse()
    {
        $elements = $this->findElements();
        if (!is_array($elements) || count($elements) === 0) {
            return $this->getSiteData();
        }

        $cssData = $this->concatCSS($elements);
        $compressed = $this->minifyCSS($cssData);

        if ($this->cutFonts === true) {
            $compressed = $this->removeFonts($compressed);
        }

        $siteData = $this->removeStyles();
        return $this->insertCompressed($compressed, $siteData);
    }

    /**
     * @param $compressed
     * @return string
     */
    private function removeFonts($compressed)
    {
        return preg_replace(
            '/url\((([^)]+(?=\.(ttf|eot|woff|woff2|odt)).\3[^)]*))\)/',
            'url(\'\')',
            $compressed
        );
    }

    /**
     * @param $compressed
     * @return string
     */
    private function modifyFonts($css, $cssLink)
    {
        preg_match_all(
            '/url\((([^)]+(?=\.(jpg|jpeg|png|gif|webp|ttf|eot|woff|woff2|odt|svg)).\3[^)]*))\)/',
            $css,
            $matches
        );

        if (count($matches[1]) === 0) {
            return $css;
        }

        $linkPath = explode('/', $cssLink);
        $toReplace = [];
        foreach ($matches[1] as $fontLink) {
            if ($this->urlNormalizer->isAbsoluteUrl($fontLink)) {
                continue;
            }
            $counter = 0;
            $fontLinkPaths = explode('/', $fontLink);
            foreach ($fontLinkPaths as $path) {
                if ($path != '..') {
                    break;
                }
                $counter++;
            }

            $newLinkPath = $linkPath;
            array_splice($newLinkPath, count($newLinkPath) - $counter - 1);
            array_splice($fontLinkPaths, 0, $counter);

            $toReplace[$fontLink] = implode('/', array_merge($newLinkPath, $fontLinkPaths));
        }

        return  str_replace(array_keys($toReplace), array_values($toReplace), $css);
    }

    /**
     * @param $compressed
     * @param $siteData
     * @return string
     */
    private function insertCompressed($compressed, $siteData)
    {
        $replaceData = '<style>' . $compressed . '</style></head>';
        return preg_replace('/<([\s]+)?\/([\s]+)?head>/', $replaceData, $siteData);
    }

    /**
     * @return string
     */
    private function removeStyles()
    {
        return str_replace($this->toRemove, '', $this->getSiteData());
    }

    /**
     * @param $data
     * @return string
     */
    private function minifyCSS($data)
    {
        $css = new CSS();
        $css->add($data);

        return $css->minify();
    }

    /**
     * @param $elements
     * @return mixed
     */
    public function concatCSS($elements)
    {
        $css = '';
        foreach ($elements[0] as $tag) {
            if (preg_match('/<link[^>]+?rel=[\'"]([\s\S]+)[\'"][\S\s^>]*?>/', $tag, $matches)) {
                if (!empty($matches[1]) && preg_match('/href=([\'"])([^\'"]+)\1/', $tag, $matches)) {
                    $this->toRemove[] = $tag;
                    $url = $this->urlNormalizer->getNormalizeUrl($matches[2]);
                    $data = $this->downloadData($url);
                    if ($data->isSuccessRequest()) {
                        $content = $data->getContent();
                        $content = $this->modifyFonts($content, $url);
                        $css .= $content;
                    }
                }
            } elseif (preg_match('/<style[\s\S]*?>([\s\S]*)<([\s]+)?\/([\s]+)?style>/', $tag, $matches)) {
                $this->toRemove[] = $tag;
                $css .= $matches[1];
            }
        }
        return $css;
    }
}
