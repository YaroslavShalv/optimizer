<?php


namespace App\Services\ParseSite\Collectors;


use App\Services\ParseSite\BaseCollector;

class ScreenShotCollector extends BaseCollector
{
    const SITE_SHOT = 1;
    const SPEED_SCREENSHOT = 2;

    /**
     * @var string
     */
    private $serviceUrl = 'https://www.site-shot.com/screenshot/';

    private $desktopConfig = [];

    private $mobileConfig = [];

    /**
     * ScreenShotCollector constructor.
     * @param array $params
     */
    public function __construct($params = [])
    {
        if (isset($params['serviceUrl'])) {
            $this->serviceUrl = $params['serviceUrl'];
        }

        if (isset($params['desktop'])) {
            $this->desktopConfig = $params['desktop'];
        }

        if (isset($params['mobile'])) {
            $this->mobileConfig = $params['mobile'];
        }

        parent::__construct($params);
    }

    /**
     * @return mixed|string
     * @throws \Exception
     */
    public function parse()
    {
        $mobileScreenShot = $this->getScreenshot($this->mobileConfig);
        sleep(1);
        $desktopScreenShot = $this->getScreenshot($this->desktopConfig);

        if ($mobileScreenShot === false || $desktopScreenShot === false) {
            throw new \Exception('Optimizator error', 500);
        }
        return $this->insertScreenshot($desktopScreenShot, $mobileScreenShot);
    }


    /**
     * @param $params
     * @return bool|string
     */
    private function getSiteShotScreenshot($params)
    {
        $params['url'] = $this->siteUrl;
        $url = $this->serviceUrl . '?' . http_build_query($params);
        $downloader = $this->downloadData($url);
        if ($downloader->getCode() != 200) {
            return false;
        }

        $json = json_decode($downloader->getContent(), true);
        if (empty($json['image'])) {
            return false;
        }

        return isset($json['image']) ? $json['image'] : false;
    }

    /**
     * @param $params
     * @return bool|string
     */
    private function getSpeedScreenshot($params)
    {
        $screenParams = [
            'site' => $this->siteUrl,
            'scale' => floor($params['scaled_width'] / $params['width'] * 100) / 100,
            'width' => $params['width'],
            'height' => $params['height'],
        ];
        $params[] = $this->siteUrl;
        $params['scale'] =
        $url = 'http://80.78.254.115/screen/screen' . '?' . http_build_query($screenParams);
        $downloader = $this->downloadData($url);
        if ($downloader->getCode() != 200) {
            return false;
        }

        $json = json_decode($downloader->getContent(), true);
        if (empty($json['screen'])) {
            return false;
        }

        return isset($json['screen']) ? 'data:image/jpeg;base64,' . $json['screen'] : false;
    }

    /**
     * @param $params
     * @return bool|string
     */
    private function getScreenshot($params)
    {
        $type = config('Screenshoter');

        $result = false;
        switch ((int) $type) {
            case self::SITE_SHOT:
                $result = $this->getSiteShotScreenshot($params);
                break;
            case self::SPEED_SCREENSHOT:
                $result = $this->getSpeedScreenshot($params);
                break;
        }

        return $result;
    }

    /**
     * @param $desktopScreenShot
     * @param $mobileScreenShot
     * @return string
     */
    public function insertScreenshot($desktopScreenShot, $mobileScreenShot)
    {
        $style = "
            .sceensave {
                position: fixed !important;
                top: 0 !important;
                left: 0 !important;
                width: 100% !important;
                height: 100% !important;
                z-index: 9999999 !important;
            }

            .sceensave img {
                width: 100%;
                object-fit: cover;
                object-position: top;
                height: 100%;
            }

            .sceensave .desk{
                display: block;
            }

            .sceensave .mob{
                display: none;
            }

            @media (max-width: 768px) {
                .sceensave .desk{
                    display: none;
                }
                .sceensave .mob{
                    display: block;
                }
            }
        ";

        $replaceData = '<style>' . $style . '</style></head>';
        $siteData = preg_replace('/<([\s]+)?\/([\s]+)?head>/', $replaceData, $this->getSiteData());
        $html = '<div class="sceensave">
                  <img class="desk" src="' . $desktopScreenShot . '">
                  <img class="mob" src="' . $mobileScreenShot . '">
                </div>';
        $siteData = preg_replace(
            '/<body([^>]*)>/',
            '$0' . $html . '<div>',
            $siteData
        );

        return preg_replace('/<[\s]*?\/[\s]*body([^>]*)>/', '</div>' . '$0', $siteData);
    }

}
