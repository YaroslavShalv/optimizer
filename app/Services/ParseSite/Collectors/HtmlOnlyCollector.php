<?php


namespace App\Services\ParseSite\Collectors;


use App\Services\ParseSite\BaseCollector;

class HtmlOnlyCollector extends BaseCollector
{
    /**
     * @var string
     */
    private $cssRegEx = '/<style[\s\S]*?>[\s\S]*?<([\s]+)?\/([\s]+)?style>|<link[^>]+?rel=[\'"]stylesheet[\'"][\S\s^>]*?>|style="([^"]+)"/';

    /**
     * @var string
     */
    private $jsRegEx = '/<script[\s\S]*?>[\s\S]*?<([\s]+)?\/([\s]+)?script>/';

    /**
     * @var string
     */
    private $imagesRegEx = '/(?:<img\s.*?src=(?:\'|"))(?:[^\'">]+)(?:\'|")(?:[^>]+)>/';

    /**
     * @var string
     */
    private $iframe = '/<iframe[\s\S]*?>[\s\S]*?<([\s]+)?\/([\s]+)?iframe>|<noscript[\s\S]*?>[\s\S]*?<([\s]+)?\/([\s]+)?noscript>/';

    /**
     * @inheritDoc
     */
    public function parse()
    {
        ini_set('pcre.backtrack_limit', '5000000');
        $siteData = $this->removeCss();
        $siteData = $this->removeJs($siteData);
        $siteData = $this->removeImages($siteData);
        $siteData = $this->removeIframes($siteData);
        return $siteData;
    }

    /**
     * @param $regExp
     * @return mixed
     */
    private function findElementsByRegExp($regExp)
    {
        preg_match_all($regExp, $this->getSiteData(), $tags);
        return $tags;
    }


    /**
     * @param $siteData
     * @return string|string[]
     */
    private function removeJs($siteData)
    {
        $elements = $this->findElementsByRegExp($this->jsRegEx);
        if (isset($elements[0]) === false) {
            return $siteData;
        }

        return str_replace($elements[0], '', $siteData);
    }

    /**
     * @return mixed
     */
    private function removeCss()
    {
        $elements = $this->findElementsByRegExp($this->cssRegEx);
        if (isset($elements[0]) === false) {
            return $this->getSiteData();
        }

        $toRemove = [];
        foreach ($elements[0] as $tag) {
            if (preg_match('/<link[^>]+?rel=[\'"]([\s\S]+)[\'"][\S\s^>]*?>/', $tag, $matches)) {
                if (!empty($matches[1]) && preg_match('/href=([\'"])([^\'"]+)\1/', $tag, $matches)) {
                    $toRemove[] = $tag;
                }
            } else {
                $toRemove[] = $tag;
            }
        }

        return str_replace($toRemove, '', $this->getSiteData());
    }

    /**
     * @param string $siteData
     * @return string|string[]
     */
    private function removeImages(string $siteData)
    {
        $elements = $this->findElementsByRegExp($this->imagesRegEx);
        if (isset($elements[0]) === false) {
            return $siteData;
        }

        return str_replace($elements[0], '', $siteData);
    }

    /**
     * @param string $siteData
     * @return string|string[]
     */
    private function removeIframes(string $siteData)
    {
        return preg_replace($this->iframe, '', $siteData);
    }
}
