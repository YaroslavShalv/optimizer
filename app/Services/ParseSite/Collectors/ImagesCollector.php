<?php

namespace App\Services\ParseSite\Collectors;

use App\Services\ParseSite\BaseCollector;
use App\Services\ParseSite\Collectors\Helpers\ImageOptimizer;

/**
 * Class ImagesCollector
 * @package App\Services\ParseSite\Collectors
 */
class ImagesCollector extends BaseCollector
{
    /**
     * @var string
     */
    protected $regEx = '/(<img\s.*?src=(?:\'|"))([^\'">]+)(\'|")/';

    /**
     * @var array
     */
    private $accessExt = ['jpg', 'jpeg', 'png', 'gif'];

    /**
     * @var bool
     */
    private $cssImagesToBase64 = false;

    /**
     * ImagesCollector constructor.
     * @param array $params
     */
    public function __construct($params = array())
    {
        $this->cssImagesToBase64 = isset($params['cssImagesToBase64']) ? (bool) $params['cssImagesToBase64'] : false;
    }

    /**
     * @inheritDoc
     */
    public function parse()
    {
        $elements = $this->findElements();
        if (!is_array($elements) || count($elements) < 2) {
            return $this->getSiteData();
        }

        $imageTags = array_combine($elements[2], $elements[1]);
        $images = array_combine($elements[2], $elements[2]);
        $base64Images = $this->convertToBase64($images);
        return $this->replaceImages($base64Images, $imageTags);
    }

    /**
     * @param array $elements
     * @param array $imageTags
     * @return string
     */
    private function replaceImages(array $elements, $imageTags)
    {
        $images = [];

        foreach ($elements as $link => $base64) {
            $tag = $imageTags[$link];
            $images[$tag . $link] = $tag . $base64;
        }

        return str_replace(array_keys($images), array_values($images), $this->getSiteData());
    }

    /**
     * @param array $images
     * @return array
     */
    private function convertToBase64(array $images)
    {
        $imageOptimizer = new ImageOptimizer();
        $base64Images = [];
        foreach ($images as $pageUrl => $image) {
            $url = $this->urlNormalizer->getNormalizeUrl($image);
            if ($this->urlNormalizer->validateUrlByExtension($url, $this->accessExt) === false) {
                continue;
            }

            $ext = $this->urlNormalizer->getExtension($image);
            $image = $this->downloadData($url);

            $content = $imageOptimizer->optimizeImage($image, $ext);
            if ($content !== false) {
                $base64Images[$pageUrl] = 'data:image/jpg;base64, ' . base64_encode($content);
            }
        }

        return $base64Images;
    }
}
