<?php

namespace App\Services\ParseSite\Collectors\Helpers;

use App\Services\ParseSite\Downloader;

class ImageOptimizer
{
    /**
     * @param Downloader $image
     * @param string $ext
     * @return bool|string
     */
    public function optimizeImage(Downloader $image, $ext)
    {
        if ($image->getCode() !== 200) {
            return  false;
        }

        $name = $this->getImagePath($ext);
        $im = imagecreatefromstring($image->getContent());
        if (!imagejpeg($im, $name, 25)) {
            return false;
        }

        $content = file_get_contents($name);
        unlink($name);
        return $content;
    }

    /**
     * @param string $ext
     * @return string
     */
    private function getImagePath($ext) : string
    {
        $name = "img_" . md5(time() . 'salt') . '.' . $ext;
        return storage_path('app/public/') . $name;
    }
}
