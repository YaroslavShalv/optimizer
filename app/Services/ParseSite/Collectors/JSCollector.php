<?php

namespace App\Services\ParseSite\Collectors;

use App\Services\ParseSite\BaseCollector;
use MatthiasMullie\Minify\JS;

class JSCollector extends BaseCollector
{
    /**
     * @var string
     */
    protected $regEx = '/<script[\s\S]*?>[\s\S]*?<([\s]+)?\/([\s]+)?script>/';

    /**
     * @var array
     */
    private $elements;

    /**
     * @inheritDoc
     */
    public function parse()
    {
        $elements = $this->findElements();
        if (!is_array($elements) || count($elements) === 0) {
            return $this->getSiteData();
        }

        $this->elements = $elements[0];

        $scriptData = $this->concatJs();
        $scriptData = $this->minifyJs($scriptData);
        $siteData = $this->removeScripts();
        return $this->insertCompressed($scriptData, $siteData);
    }

    /**
     * @param $compressed
     * @param $siteData
     * @return string
     */
    private function insertCompressed($compressed, $siteData)
    {
        $replaceData = '<script>' . $compressed . '</script></body>';
        $replaceKey = 'REGEXPBLOCK';
        $siteData = preg_replace('/<(?:[\s]+)?\/(?:[\s]+)?body>/', $replaceKey, $siteData);
        return str_replace($replaceKey, $replaceData, $siteData);
    }

    /**
     * @return string
     */
    private function removeScripts()
    {
        return str_replace($this->elements, '', $this->getSiteData());
    }

    /**
     * @param $data
     * @return string
     */
    private function minifyJs($data)
    {
        $js = new JS();
        $js->add($data);

        return $js->minify();
    }

    /**
     * @return mixed
     */
    public function concatJs()
    {
        $scripts = [];
        foreach ($this->elements as $k => $tag) {
            if (preg_match('/<script[^>]*src=?[\'"]([^\'"]+)+[\'"]?/', $tag, $matches)) {
                if (!empty($matches[1])) {
                    $data = $this->downloadData($this->urlNormalizer->getNormalizeUrl($matches[1]));
                    if (!$data->isSuccessRequest()) {
                        unset($this->elements[$k]);
                        continue;
                    }
                    $content = $data->getContent();
                    if ($this->isJson($content)) {
                        unset($this->elements[$k]);
                    } else {
                        $scripts[] = $data->getContent();
                    }
                }
            } elseif (preg_match('/<script[\s\S]*?>([\s\S]*)<([\s]+)?\/([\s]+)?script>/', $tag, $matches)) {
                $content = $matches[1];
                if ($this->isJson($content)) {
                    unset($this->elements[$k]);
                } else {
                    $scripts[] = $content;
                }
            }
        }
        return implode('', $scripts);
    }

    /**
     * @param string $data
     * @return bool
     */
    private function isJson($data)
    {
        @json_decode($data);
        return (json_last_error() === JSON_ERROR_NONE);
    }
}
