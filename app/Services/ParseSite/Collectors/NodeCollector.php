<?php

namespace App\Services\ParseSite\Collectors;

use App\Services\ParseSite\BaseCollector;
use Exception;
use Illuminate\Support\Facades\Log;

/**
 * Class NodeCollector
 * @package App\Services\ParseSite\Collectors
 */
class NodeCollector extends BaseCollector
{
    /**
     * @var mixed
     */
    private $serviceUrl;

    /**
     * NodeCollector constructor.
     * @param array $params
     */
    public function __construct($params = array())
    {
        if (isset($params['serviceUrl'])) {
            $this->serviceUrl = $params['serviceUrl'];
        }

        parent::__construct($params);
    }

    /**
     * @return mixed|void
     * @throws Exception
     */
    public function parse()
    {
        $html = $this->getOptimizedHtml();
        if ($html === false) {
            throw new \Exception('Node Optimizator error', 500);
        }

        return $html;
    }

    /**
     * @return false|mixed
     */
    private function getOptimizedHtml(): string
    {
        $params['site'] = $this->siteUrl;
        $url = $this->serviceUrl . '?' . http_build_query($params);
        $downloader = $this->downloadData($url);
        if ($downloader->getCode() != 200) {
            Log::error('Json decode exception node result', [
                'body' => var_export([
                    'content' => $downloader->getContent(),
                    'error' => $downloader->getErrMsg(),
                ], true),
            ]);
            return false;
        }

        try {
            $json = json_decode($downloader->getContent(), true);
        } catch (Exception $e) {
            Log::error('Json decode exception node result', [
                'body' => $e->getMessage(),
            ]);
            return false;
        }

        return $json['html'] ?? false;
    }
}
