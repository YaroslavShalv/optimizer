<?php

namespace App\Services\ParseSite;

/**
 * Class Downloader
 */
class Downloader
{
    const SUCCESS_CODE = 200;

    /**
     * @var array
     */
    private $result = [];

    /**
     * @return string
     */
    public function getContent()
    {
        return isset($this->result['content']) ? $this->result['content'] : '';
    }

    /**
     * @return string
     */
    public function getErrno()
    {
        return isset($this->result['errno']) ? $this->result['errno'] : '';
    }

    /**
     * @return string
     */
    public function getErrMsg()
    {
        return isset($this->result['errmsg']) ? $this->result['errmsg'] : '';
    }

    /**
     * @return string
     */
    public function getHeaders()
    {
        return isset($this->result['headers']) ? $this->result['headers'] : '';
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->result;
    }

    /**
     * @return mixed|string
     */
    public function getCode()
    {
        return isset($this->result['code']) ? $this->result['code'] : '';
    }

    /**
     * @return bool
     */
    public function isSuccessRequest()
    {
        return (int) $this->getCode() === self::SUCCESS_CODE;
    }

    /**
     * @param string $url
     * @param array $options
     * @return Downloader
     */
    public function download($url, $options = [])
    {
        if (preg_match('/(.*)\?(.*)/', $url)) {
            $url .= '&optimizer=optimizer';
        } else {
            $url .= '?optimizer=optimizer';
        }

        $params = [
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POST => false,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0',
            CURLOPT_COOKIEFILE => "cookie.txt",
            CURLOPT_COOKIEJAR => "cookie.txt",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_AUTOREFERER => true,
            CURLOPT_CONNECTTIMEOUT => 120,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_VERBOSE => false,
        ];

        $params = array_replace($params, $options);

        $result = [];
        $ch = curl_init($url);
        curl_setopt_array($ch, $params);
        $result['content'] = curl_exec($ch);
        $result['errno'] = curl_errno($ch);
        $result['errmsg'] = curl_error($ch);
        $result['headers'] = curl_getinfo($ch);
        $result['code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->result = $result;

        return $this;
    }
}
