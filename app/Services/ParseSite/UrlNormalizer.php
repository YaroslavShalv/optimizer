<?php

namespace App\Services\ParseSite;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class UrlNormalizer
 * @package App\Services\ParseSite
 */
class UrlNormalizer
{
    /**
     * @var string
     */
    private $baseDomain;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * UrlNormalizer constructor.
     * @param $baseUrl
     */
    public function __construct($baseUrl)
    {
        $this->baseUrl = rtrim($baseUrl, "/");;
        $this->baseDomain = $this->getDomainByUrl($baseUrl);
    }

    /**
     * @param $url
     * @return string
     */
    public function getNormalizeUrl($url)
    {
        if ($this->isAbsoluteUrl($url)) {
            return $url;
        }

        if (substr($url, 0, 1) == "/") {
            return $this->baseDomain . $url;
        }

        return $this->baseUrl . "/" . $url;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->baseDomain;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }


    /**
     * @param $url
     * @return false|int
     */
    public function isAbsoluteUrl($url)
    {
        $regexp = '/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/';
        return preg_match($regexp, $url);
    }

    /**
     * @param $url
     * @return string
     */
    public function getDomainByUrl($url)
    {
        if ($this->isAbsoluteUrl($url) === false) {
            throw new BadRequestHttpException('Can\'t get domain from non absolute url');
        }

        $regex = '/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|\/\/)?([A-Za-z0-9.]+)[\/]?/';
        if (preg_match($regex, $url, $matches) === false) {
            throw new BadRequestHttpException('Wrong url');
        }

        if ($matches[1] === '//') {
            $matches[1] = 'https:' . $matches[1];
        }

        array_shift($matches);

        return implode('', $matches);
    }

    /**
     * @param string $url
     * @return mixed
     */
    public function getExtension($url)
    {
        $url = explode('?', $url);
        $url = explode('.', $url[0]);
        return end($url);
    }


    /**
     * @param string $url
     * @param string|array $ext
     * @return mixed
     */
    public function validateUrlByExtension($url, $ext)
    {
        $urlExtension = $this->getExtension($url);
        return is_array($ext) ? in_array($urlExtension, $ext) : $urlExtension === $ext;
    }
}
