<?php

namespace App\Services;

use App\Services\ParseSite\BaseCollector;
use App\Services\ParseSite\Collectors\NodeCollector;
use App\Services\ParseSite\Downloader;
use Illuminate\Support\Facades\Log;

/**
 * Class ParseSiteService
 * @package App\Services
 */
class ParseSiteService
{
    /**
     * @var string
     */
    private $siteData = '';

    /**
     * @var string
     */
    private $siteUrl;

    /**
     * @var ParseSiteService
     */
    private $configurator;

    /**
     * @var BaseCollector[]
     */
    private $collectors;

    /**
     * @var bool
     */
    private $downloadNeeded = false;

    /**
     * ParseSiteService constructor.
     * @param $url
     * @param $publicKey
     */
    public function __construct($url, $publicKey)
    {
        $this->siteUrl = $url;
        $this->configurator = new ConfiguratorService($publicKey);
        $this->setCollectors();
        if ($this->downloadNeeded) {
            $this->downloadSiteData();
        }
    }

    /**
     * @return void
     */
    private function downloadSiteData()
    {
        Log::debug('Start site download');
        $downloader = new Downloader();
        $data = $downloader->download($this->siteUrl);
        $this->siteData = $data->getContent();
        Log::debug('Start download finish');
    }


    /**
     * @return mixed|string
     */
    public function optimizeSite()
    {
        foreach ($this->collectors as $collector) {
            Log::debug('Start collector');
            $collector->setSiteData($this->siteUrl, $this->siteData);
            $this->siteData = $collector->parse();
        }
        return $this->siteData;
    }


    private function setCollectors()
    {
        foreach ($this->configurator->getComponents() as $collector) {
            if ($collector instanceof NodeCollector) {
                $this->collectors = [$collector];
                return;
            }
        }
        $this->downloadNeeded = true;
        $this->collectors = $this->configurator->getComponents();
    }
}
