<?php

namespace App\Services\Security;

use App\Models\Site;
use App\Services\Security\Exceptions\SecurityExceptionWrongLink;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Services\Security\Exceptions\SecurityException;
use App\Services\Security\Exceptions\SecurityExceptionWrongDomain;

/**
 * Class SecurityCheckService
 * @package App\Services
 */
class SecurityCheckService
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var string
     */
    private $publicKey;

    /**
     * @var string
     */
    private $signature;

    /**
     * @var Model
     */
    private $model;

    /**
     * @var string
     */
    private $publicModelParam;

    /**
     * @var string
     */
    private $privateModelParam;

    /**
     * @var string
     */
    private $regExp = "/^OPTIMIZER\s([a-zA-Z0-9]{5,})\:([\S]{5,})$/";

    /**
     * SecurityCheckService constructor.
     * @param Request $request
     * @param Model $model
     * @param string $publicKeyParam
     * @param string $privateKeyParam
     */
    public function __construct(Request $request, Model $model, $publicKeyParam, $privateKeyParam)
    {
        $this->request = $request;
        $this->model = $model;
        $this->publicModelParam = $publicKeyParam;
        $this->privateModelParam = $privateKeyParam;
    }

    /**
     * @throws SecurityException
     */
    public function validateSignature()
    {
        $this->setAuthParams();
        $params = $this->getParams();
        $keys = $this->getKeys();

        $method = $this->request->getMethod();
        ksort($params);
        $md5Params = md5(json_encode($params));

        $contentType = $this->request->header('Content-type');
        $stringToSign = implode('+', [
            $method,
            $md5Params,
            $contentType,
            $this->getTimeHeader(),
        ]);

        $signature = base64_encode(hash_hmac(
            'sha256',
            $stringToSign,
            $keys->{$this->privateModelParam},
            true
        ));

        if ($this->signature !== $signature) {
            Log::error('signature error');
            throw new SecurityException();
        }

        if (isset($keys->url) && strpos($keys->url, ltrim($params['host'], 'www.')) === false) {
            Log::error('url error');
            throw new SecurityExceptionWrongDomain();
        }

        if (isset($params['host']) && isset($params['link']) && strpos($params['link'], $params['host']) === false) {
            Log::error('link error');
            throw new SecurityExceptionWrongLink();
        }
    }

    /**
     * @return resource|string
     */
    private function getParams()
    {
        return json_decode($this->request->getContent(), true);
    }

    /**
     * @throws SecurityException
     * @return Model|Site
     */
    private function getKeys()
    {
        $exists = $this->model::query()->where([
            $this->publicModelParam => $this->publicKey,
            'status' => 1
        ])->first();

        if ($exists === null) {
            throw new SecurityException();
        }

        return $exists;
    }

    /**
     * @throws SecurityException
     */
    private function setAuthParams()
    {
        preg_match($this->regExp, $this->getAuthString(), $matches);
        if (!isset($matches[1]) || strlen($matches[1]) < 5) {
            Log::error('Unauthorized action. Public key error.');
            throw new SecurityException();
        } else {
            $this->publicKey = $matches[1];
        }

        if (!isset($matches[2]) || strlen($matches[2]) < 5) {
            Log::error('Unauthorized action. Signature error.');
            throw new SecurityException();
        } else {
            $this->signature = $matches[2];
        }
    }


    /**
     * @return string
     */
    private function getAuthString() : string
    {
        return $this->request->header('Optimizer-Auth');
    }

    /**
     * @return array|string|null
     */
    private function getTimeHeader() : string
    {
        return $this->request->header('REQUEST_TIME');
    }
}
