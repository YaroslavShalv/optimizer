<?php

namespace App\Services\Security\Exceptions;

/**
 * Class SecurityException
 * @package App\Services\Security\Exceptions
 */
class SecurityException extends \Exception
{
    /**
     * @var string
     */
    public $code = '403';

    /**
     * @var string
     */
    public $message = "Permission denied!";
}
