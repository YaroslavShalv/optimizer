<?php

namespace App\Services\Security\Exceptions;


class SecurityExceptionWrongLink extends SecurityException
{
    /**
     * @var string
     */
    public $code = '403';

    /**
     * @var string
     */
    public $message = "Permission denied!";
}
