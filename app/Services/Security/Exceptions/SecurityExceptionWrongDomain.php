<?php

namespace App\Services\Security\Exceptions;


class SecurityExceptionWrongDomain extends SecurityException
{
    /**
     * @var string
     */
    public $code = '403';

    /**
     * @var string
     */
    public $message = "Permission denied!";
}
