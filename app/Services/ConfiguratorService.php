<?php

namespace App\Services;

use App\Models\Configurator;
use App\Models\ConfiguratorsSite;
use App\Models\Site;
use App\Services\ParseSite\BaseCollector;

/**
 * Class ConfiguratorService
 * @package App\Services
 */
class ConfiguratorService
{
    /**
     * @var string
     */
    private $publicKey;

    /**
     * ConfiguratorService constructor.
     * @param string $publicKey
     */
    public function __construct($publicKey)
    {
        $this->publicKey = $publicKey;
    }

    /**
     * @return BaseCollector[]|array
     */
    public function getComponents()
    {
        $services = config('services.configuratorService');
        $adminConfigurations = $this->getConfigurations();
        if (is_array($services) === false) {
            return [];
        }

        $collectors = [];
        foreach ($services as $key => $service) {
            if ((bool)$adminConfigurations[$key] === false) {
                continue;
            }

            $params = isset($service['params']) ? $service['params'] : [];
            foreach ($params as $key => $param) {
                $params[$key] = isset($adminConfigurations[$key]) ? $adminConfigurations[$key] : $param;
            }
            if (isset($service['class']) && class_exists($service['class'])) {
                $collectors[$service['class']] = new $service['class']($params);
            }
        }

        return $collectors;
    }

    /**
     * @return array
     */
    public function getConfigurations()
    {
        $siteConfigurations = $this->getSiteConfiguration();
        $defaultConfigurations = $this->getDefaultConfiguration();

        $configurations = [];
        foreach ($defaultConfigurations as $configuration) {
            $value = isset($siteConfigurations[$configuration->id])
                ? $siteConfigurations[$configuration->id]
                : $configuration->defaultValue;
            $configurations[$configuration->key] = $value;
        }

        return $configurations;
    }

    /**
     * @return array
     */
    private function getSiteConfiguration(): array
    {
        $siteId = $this->getSiteId();
        if ($siteId === null) {
            return [];
        }

        $config = ConfiguratorsSite::query()->where([
            'site_id' => $siteId,
        ])->get();

        return $config->mapWithKeys(function (ConfiguratorsSite $model) {
            return [$model->configurator_id => $model->value];
        })->all();
    }

    /**
     * @return Configurator[]|[]
     */
    private function getDefaultConfiguration()
    {
        return Configurator::query()->scopes('active')->get()->all();
    }

    /**
     * @return int|null
     */
    private function getSiteId()
    {
        $site = Site::query()->where([
            'public_key' => $this->publicKey,
            'status' => 1,
        ])->first();

        return $site === null ? null : $site->id;
    }
}
