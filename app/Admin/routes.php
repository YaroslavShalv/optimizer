<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->get('/optimizer', 'OptimizerController@index')->name('admin.optimizer.home');
    $router->resource('/user', 'UserController');
    $router->resource('/user-agent', 'UserAgentController');
    $router->post('/site/generatePrivateKey', 'SiteController@generatePrivateKey')->name('admin.site.generatePrivateKey');
    $router->post('/site/generatePublicKey', 'SiteController@generatePublicKey')->name('admin.site.generatePublicKey');
    $router->get('/site/downloadZip/{id}', 'SiteController@downloadZip')->name('admin.site.downloadZip');
    $router->resource('/site', 'SiteController');
    $router->get('/siteUrl/index/{id}', 'SiteUrlController@indexGrid');
    $router->get('/siteUrlLog/index/{id}', 'SiteUrlLogController@indexGrid');
    $router->get('/siteUrlLog/show/{id}', 'SiteUrlLogController@show');
    $router->any('/site/config/{id}', 'SiteController@config')->name('admin.site.config');
    $router->resource('/configurator', 'ConfiguratorController');
    $router->get('/logger/clearLog', 'LoggerController@clearLog');
    $router->resource('/logger', 'LoggerController');
});
