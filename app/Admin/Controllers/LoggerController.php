<?php

namespace App\Admin\Controllers;

use App\Models\Log;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class LoggerController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Log';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Log());

        $grid->header(function (){
            $link = url('admin/logger/clearLog');
            return '<button class="btn btn-primary" >
                        <a style="color:white;" href="' . $link . '">Clear log</a>
                    </button>';
        });

        $grid->option('show_tools', false);
        $grid->option('show_tools', false);
        $grid->option('show_actions', false);
        $grid->option('show_create_btn', false);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('message', 'Message')->display(function ($message) {
            $url = url('admin/logger/' . $this->id);
            return '<a href="' . $url . '">' . $message . '</a>';
        });
        $grid->column('channel', __('Channel'))->sortable();
        $grid->column('level', 'Level')
            ->filter(Log::$levels)
            ->display(function ($level) {
                return isset(Log::$levels[$level]) ? Log::$levels[$level] : $level;
            });
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->model()->orderBy('id', 'desc');

        return $grid;
    }

    public function clearLog()
    {
        Log::truncate();
        return redirect('admin/logger');
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Log::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('message', __('Message'));
        $show->field('channel', __('Channel'));
        $show->field('level', __('Level'))->using(Log::$levels);
        $show->field('level_name', __('Level Name'));
        $show->field('unix_time', __('Unix Time'));
        $show->field('datetime', __('Datetime'))->as(function ($value) {
            try {
                return unserialize($value)->date;
            } catch (\Exception $exception) {
                return  '';
            }
        });
        $show->field('context', __('Context'))->json();
        $show->field('extra', __('Extra'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }


    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return new Form(new Log());
    }
}
