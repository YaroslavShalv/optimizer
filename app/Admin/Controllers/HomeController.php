<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        $clientView = '';
        if (Admin::getCurrentUser()->isAdmin() === false) {
            $clientView = view('admin/home/dashboard_client');
        }

        return $content
            ->title('Admin panel')
            ->row('Hello, dear client!')
            ->row(function (Row $row) use ($clientView) {
                if (Admin::getCurrentUser()->isAdmin()) {
                    $row->column(4, function (Column $column) {
                        $column->append(Dashboard::environment());
                    });

                    $row->column(4, function (Column $column) {
                        $column->append(Dashboard::extensions());
                    });

                    $row->column(4, function (Column $column) {
                        $column->append(Dashboard::dependencies());
                    });
                } else {
                    $row->column(12, function (Column $column) use ($clientView) {
                        $column->append($clientView);
                    });
                }
            });
    }
}
