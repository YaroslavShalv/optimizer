<?php

namespace App\Admin\Controllers;

use App\Exceptions\PermissionExceptions;
use App\Models\Admin;
use App\Models\Configurator;
use App\Models\ConfiguratorsSite;
use App\Models\Site;
use App\Services\StringGenerator;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpKernel\Exception\HttpException;
use function foo\func;

class SiteController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Site';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Site());

        if (Admin::getCurrentUser()->isAdmin() === false) {
            $grid->model()->where('user_id', '=', Admin::getCurrentUser()->id);

            $grid->disableCreateButton(true);
            $grid->disableActions(true);
        }

        $grid->column('id', __('ID'))->sortable();
        $grid->column('url', __('Site'))->display(function ($url) {
            $url = url('admin/siteUrl/index', ['id' => $this->id]);
            return '<a href="' . $url . '">' . $this->url . '</a>';
        })->sortable();
        $grid->column('public_key', __('Public Key'))->sortable();
        $grid->column('private_key', __('Private Key'))->sortable();
        $grid->column('status', __('Status'))
            ->display(function ($status) {
                $statuses = Site::statusArray();
                return isset($statuses[$status]) ? $statuses[$status] : $status;
            });

        if (Admin::getCurrentUser()->isAdmin()) {
            $grid->column('Configs', 'Configs')->display(function ($config) {
                $url = url('admin/site/config', ['id' => $this->id]);
                return '<a href="' . $url . '">Edit configs</a>';
            });

            $grid->column('Files', 'Files')->display(function ($model) {
                $url = url('admin/site/downloadZip', ['id' => $this->id]);
                return '<a target="_blank"  href="' . $url . '">Download zip</a>';
            });
        }
        $grid->column('pay_status', __('Pay Status'))
            ->display(function ($status) {
                $statuses = Site::payStatusArray();
                return isset($statuses[$status]) ? $statuses[$status] : $status;
            });
        $grid->column('user_id', __('User'))
            ->display(function ($userId) {
                $users = Admin::getUserList();
                return isset($users[$userId]) ? $users[$userId] : $userId;
            });
        $grid->column('Cache', 'Cache')->display(function ($model) {
            $url = $this->url . '?optimizerClearCache=' . $this->public_key;
            return '<a target="_blank"  href="' . $url . '">Clear cache</a>';
        });
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->paginate(200);
        $grid->model()->orderBy('id', 'desc');
        $grid->filter(function ($filter) {
            $filter->like('url', 'Url');
            $filter->in('pay_status', 'Pay status')->multipleSelect(Site::payStatusArray());
            $filter->in('status', 'Status')->multipleSelect(Site::statusArray());
            $filter->in('user_id', 'User')->multipleSelect(Admin::getUserList());
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     * @throws PermissionExceptions
     */
    protected function detail($id)
    {
        $model = Site::findOrFail($id);

        if (Admin::getCurrentUser()->isAdmin() === false) {
            throw new PermissionExceptions();
        }

        $show = new Show($model);

        $show->field('id', __('ID'));
        $show->field('url', __('Site'));
        $show->field('public_key', __('Public Key'));
        $show->field('private_key', __('Private Key'));
        $show->field('status', __('Status'))->using(Site::statusArray());
        $show->field('pay_status', __('Pay Status'))->using(Site::payStatusArray());
        $show->field('requisites', __('Requisites'));
        $show->field('user_id', __('User'))->using(Admin::getUserList());
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $userId = null;
        $form = new Form(new Site());

        if (Admin::getCurrentUser()->isAdmin() === false) {
            throw new PermissionExceptions();
        }

        \Encore\Admin\Facades\Admin::js(URL::to('js/keyGenerator.js'));
        $urlPrivate = url('admin/site/generatePrivateKey');
        $urlPublic = url('admin/site/generatePublicKey');
        $form->display('id', __('ID'));
        $form->url('url', __('Site'));
        $form->text('public_key', __('Public Key'))
            ->append('<span data-url="' . $urlPublic . '" id="generatePublic" class="btn-sm btn-primary">Generate</span>');
        $form->text('private_key', __('Private Key'))
            ->append('<span  data-url="' . $urlPrivate . '"  id="generatePrivate" class="btn-sm btn-primary">Generate</span>');
        $form->radio('status', __('Status'))->options(Site::statusArray())->default(1);
        $form->select('pay_status', __('Pay status'))
            ->options(Site::payStatusArray())->default(Site::STATUS_PENDING);
        $form->select('user_id', __('User'))->options(Admin::getUserList());
        $form->textarea('requisites');
        $form->display('created_at', __('Created At'));
        $form->display('updated_at', __('Updated At'));
        return $form;
    }

    /**
     * @param int $id
     * @param Content $content
     * @param Request $request
     */
    public function downloadZip($id, Content $content, Request $request)
    {
        $site = Site::query()->where(['id' => $id])->get()->first();
        if ($site === null) {
            throw new HttpException(404, 'Site not found');
        }

        $name = trim(preg_replace('/[^\da-zA-Z.]+/', '_', $site->url), '_');
        $zip_file = storage_path('app/public') .  $name . '.zip';

        $zip = new \ZipArchive();
        $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $path = storage_path() . '/../' . 'optimizerClient_5.4';
        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
        foreach ($files as $name => $file) {
            if ($file->isDir()) {
                continue;
            }

            if ($file->getFilename() === 'index.php') {
                $indexFile = file_get_contents($files->getRealPath());
                $indexFile = str_replace('{{public}}', $site->public_key, $indexFile);
                $indexFile = str_replace('{{private}}', $site->private_key, $indexFile);

                $zip->addFromString('index.php', $indexFile);
                continue;
            }

            $filePath = $file->getRealPath();
            $relativePath = $file->getFilename();
            $zip->addFile($filePath, $relativePath);
        }
        $zip->addEmptyDir('pagesCache');
        $zip->close();
        return response()->download($zip_file);
    }

    /**
     * @param Content $content
     * @param Request $request
     */
    public function generatePrivateKey(Content $content, Request $request)
    {
        do {
            $randomString = StringGenerator::generateRandomString(16);
            $exist = Site::query()->where(['private_key' => $randomString])->get()->all();
        } while(count($exist) !== 0);

        echo json_encode(['key' => $randomString]);
        exit();
    }

    /**
     * @param Content $content
     * @param Request $request
     */
    public function generatePublicKey(Content $content, Request $request)
    {
        do {
            $randomString = StringGenerator::generateRandomString(12);
            $exist = Site::query()->where(['public_key' => $randomString])->get()->all();
        } while(count($exist) !== 0);

        echo json_encode(['key' => $randomString]);
        exit();
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @param Request $request
     * @return Content
     */
    public function config($id, Content $content, Request $request)
    {
        if (Admin::getCurrentUser()->isAdmin() === false) {
            throw new PermissionExceptions();
        }

        $defaultConfig = Configurator::query()->scopes('active')->get()->all();
        $siteConfigs = ConfiguratorsSite::query()->where(['site_id' => $id])->get()->all();

        /** @var ConfiguratorsSite[] $siteConfigs */
        $siteConfigs = collect($siteConfigs)->mapWithKeys(function (ConfiguratorsSite $model) {
            return [$model->configurator_id => $model];
        })->all();

        if ($request->isMethod('post')) {
            foreach ($request->post('configurator', []) as $configId => $value) {
                $value = (int) $value === 1 ? 1 : 0;
                if (isset($siteConfigs[$configId])) {
                    $siteConfigs[$configId]->value = $value;
                    $siteConfigs[$configId]->save();
                    continue;
                }

                $config = new ConfiguratorsSite();
                $config->site_id = $id;
                $config->configurator_id = $configId;
                $config->value = $value;
                $config->save();
                $siteConfigs[$configId] = $config;
            }
            return redirect('/admin/site');
        }

        return $content
            ->title($this->title())
            ->description($this->description['show'] ?? trans('admin.show'))
            ->body(view('admin/site/config', [
                'defaultConfig' => $defaultConfig,
                'siteConfigs' => $siteConfigs,
            ]));

    }
}
