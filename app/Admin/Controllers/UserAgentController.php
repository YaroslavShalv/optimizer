<?php

namespace App\Admin\Controllers;

use App\Models\UserAgent;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UserAgentController extends AdminController
{
    /**
     * @return Grid
     */
    public function grid()
    {
        $grid = new Grid(new UserAgent());

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name', __('Name'))->sortable()->searchable();
        $grid->column('regexp', __('Regexp'))->sortable()->searchable();
        $grid->column('comment', __('Comment'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(UserAgent::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name', __('Name'));
        $show->field('regexp', __('Regexp'));
        $show->field('comment', __('Comment'));
        $show->field('status', __('Status'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new UserAgent);

        $form->display('id', __('ID'));
        $form->text('name', __('Name'));
        $form->text('regexp', __('Regexp'));
        $form->text('comment', __('Comment'));
        $form->radio('status', __('Status'))
            ->options([0 => 'Off', 1 => 'On'])
            ->default(1);
        $form->display('created_at', __('Created At'));
        $form->display('updated_at', __('Updated At'));

        return $form;
    }
}
