<?php

namespace App\Admin\Controllers;

use App\Exceptions\PermissionExceptions;
use App\Models\Admin;
use App\Models\Configurator;
use App\Models\Site;
use App\Models\SiteUrl;
use App\Models\SiteUrlLog;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;

/**
 * Class SiteUrlController
 * @package App\Admin\Controllers
 */
class SiteUrlController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'SiteUrl';

    /**
     * Make a grid builder.
     * @param $siteId
     *
     * @return Grid
     */
    protected function grid($siteId)
    {
        $site = Site::findOrFail($siteId);
        if (Admin::getCurrentUser()->isAdmin() === false && $site->user_id != Admin::getCurrentUser()->id) {
            throw new PermissionExceptions();
        }

        $grid = new Grid(new SiteUrl());
        $grid->model()->where('site_id', '=', $siteId);
        $grid->column('id', __('ID'))->sortable();
        $grid->column('url', __('Site'))->display(function ($url) {
            $url = url('admin/siteUrlLog/index', ['id' => $this->id]);
            return '<a target="_blank"  href="' . $url . '">' . $this->url . '</a>';
        })->sortable();
        $grid->column('status', __('Status'))
            ->filter(SiteUrl::statusArray())
            ->display(function ($status) {
                $statuses = SiteUrl::statusArray();
                return isset($statuses[$status]) ? $statuses[$status] : $status;
            });
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->paginate(50);
        $grid->disableActions(true);

        return $grid;
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @param int $siteId
     *
     * @return Content
     */
    public function indexGrid(Content $content, $siteId)
    {
        return $content
            ->title($this->title())
            ->description($this->description['index'] ?? trans('admin.list'))
            ->body($this->grid($siteId));
    }

}
