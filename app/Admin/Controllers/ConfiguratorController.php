<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use App\Models\Configurator;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ConfiguratorController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Default configurator';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Configurator());

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name', __('Name'))
            ->sortable();
        $grid->column('defaultValue', __('Default Value'))
            ->filter(Configurator::statusArray())
            ->select(Configurator::statusArray())->sortable();
        $grid->column('key', __('Key'))
            ->sortable();
//        $grid->column('type', __('Type'))
//            ->filter(Configurator::getTypes())
//            ->sortable();
        $grid->column('status', __('Status'))
            ->filter(Configurator::statusArray())
            ->select(Configurator::statusArray())->sortable();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Configurator::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name', __('Name'));
        $show->field('defaultValue', __('Default Value'))->using(Configurator::statusArray());
        $show->field('key', __('Key'));
//        $show->field('type', __('Type'))->using(Configurator::getTypes());
        $show->field('status', __('Status'))->using(Configurator::statusArray());
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Configurator());

        $form->display('id', __('ID'));
        $form->text('name', __('Name'));
        $form->text('defaultValue', __('Default Value'));
        $form->text('key', __('Key'));
        $form->select('type', __('Type'))
            ->options(Configurator::getTypes())
            ->default(Configurator::TYPE_CHECKBOX);
        $form->radio('status', __('Status'))
            ->options(Configurator::statusArray())
            ->default(1);
        $form->display('created_at', __('Created At'));
        $form->display('updated_at', __('Updated At'));

        return $form;
    }
}
