<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ConfiguratorService;
use App\Services\ParseSiteService;

/**
 * Class OptimizerController
 * @package App\Admin\Controllers
 * @deprecated used only for test
 */
class OptimizerController extends Controller
{
    public function index()
    {
        $url = !empty($_GET['url']) ? $_GET['url'] : 'http://bm-tour.com/';
        $service = new ParseSiteService($url, 'bmtour');
        echo $service->optimizeSite();
//        $configurator = new ConfiguratorService('bsfqxOQ1lqsA123');
//        $services = $configurator->getComponents();
//        var_dump($services);
//        die();


    }

}
