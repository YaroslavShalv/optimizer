<?php

namespace App\Admin\Controllers;

use App\Exceptions\PermissionExceptions;
use App\Models\Admin;
use App\Models\Site;
use App\Models\SiteUrl;
use App\Models\SiteUrlLog;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

/**
 * Class SiteUrlLogController
 * @package App\Admin\Controllers
 */
class SiteUrlLogController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Site Url Log';

    /**
     * Make a grid builder.
     * @param $siteUrlId
     *
     * @return Grid
     */
    protected function grid($siteUrlId)
    {
        $siteUrl = SiteUrl::findOrFail($siteUrlId);
        if (Admin::getCurrentUser()->isAdmin() === false && $siteUrl->site->user_id != Admin::getCurrentUser()->id) {
            throw new PermissionExceptions();
        }

        $grid = new Grid(new SiteUrlLog());

        $grid->model()->where('site_url_id', '=', $siteUrlId);
        $grid->column('id', __('ID'))->sortable();
        $grid->column('url', __('Site'))->display(function ($model) {
            $url = url('admin/siteUrlLog/show', ['id' => $this->id]);
            return '<a target="_blank"  href="' . $url . '">Show log</a>';
        })->sortable();
        $grid->column('status', __('Status'))
            ->filter(SiteUrlLog::statusArray())
            ->display(function ($status) {
                $statuses = SiteUrlLog::statusArray();
                return isset($statuses[$status]) ? $statuses[$status] : $status;
            });
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->paginate(50);
        $grid->disableActions(true);
        $grid->disableCreateButton(true);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SiteUrlLog::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('url', __('Site'));
        $show->field('public_key', __('Public Key'));
        $show->field('private_key', __('Private Key'));
        $show->field('status', __('Status'))->using(Site::statusArray());
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @param int $siteUrlId
     *
     * @return Content
     */
    public function indexGrid(Content $content, $siteUrlId)
    {
        return $content
            ->title($this->title())
            ->description($this->description['index'] ?? trans('admin.list'))
            ->body($this->grid($siteUrlId));
    }
}
