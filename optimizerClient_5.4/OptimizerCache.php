<?php

class OptimizerCache
{
    const TYPE_JSON = 'json';
    const TYPE_PAGE = 'page';

    /** @var int
     * 0 means that cache is infinity
     */
    const PAGE_LIFE_TIME = 0;
    const JSON_LIFE_TIME = 3600;

    /**
     * @var string
     */
    private $config;

    /**
     * @var string
     */
    private $path;

    public function __construct()
    {
        $this->config = __DIR__ . '/pagesCache/config.json';
        $this->path = __DIR__ . '/pagesCache/';
    }

    /**
     * @param Downloader $downloader
     * @param string $name
     * @return string
     */
    public function saveCache(Downloader $downloader, $name)
    {
        if ($downloader->getCode() !== 200) {
            return '';
        };
        $this->saveCacheFile($name);
        file_put_contents($this->path . $name, $downloader->getContent());
        return $downloader->getContent();
    }

    /**
     * @param string $file
     * @return bool|string
     */
    public function getCache($file)
    {
        if ($this->checkFileIsExpired($file)) {
            return  false;
        }

        if (file_exists($this->path . $file)) {
            return file_get_contents($this->path . $file);
        }
        return false;
    }

    /**
     * @param string $file
     * @return string
     */
    public function getFileType($file)
    {
        $ext = pathinfo($this->path . $file, PATHINFO_EXTENSION);
        if ($ext === 'json') {
            return self::TYPE_JSON;
        } else {
            return self::TYPE_PAGE;
        }
    }

    /**
     * @param string $file
     * @return bool
     */
    private function checkFileIsExpired($file)
    {
        $config = $this->getCacheConfig();
        if (empty($config[$file])) {
            return true;
        }

        $type = $this->getFileType($file);
        $time = $this->getTime($type);

        return $time === 0 ? false : $config[$file] + $time < time();
    }

    /**
     * @return bool|string
     */
    private function getCacheConfig()
    {
        if (!file_exists($this->config)) {
            return false;
        }

        $config = file_get_contents($this->config);
        if (empty($config)) {
            return false;
        }

        return json_decode($config, true);
    }

    /**
     * @param string $file
     */
    private function saveCacheFile($file)
    {
        $config = $this->getCacheConfig();
        if (!is_array($config)) {
            $config = [];
        }

        $config[$file] = time();
        file_put_contents($this->config, json_encode($config));
    }


    /**
     * @param string $type
     * @return float|int
     */
    private function getTime($type)
    {
        $time = 0;

        switch ($type) {
            case self::TYPE_JSON : $time = self::JSON_LIFE_TIME;
                break;
            case self::TYPE_PAGE : $time = self::PAGE_LIFE_TIME;
                break;
        }

        return $time;
    }

    /**
     * @return void
     */
    public function clearCache()
    {
        $files = scandir($this->path);
        $exclude = ['.', '..'];
        foreach ($files as $file) {
            if (in_array($file, $exclude)) {
                continue;
            }
            unlink($this->path . $file);
        }
    }

}
