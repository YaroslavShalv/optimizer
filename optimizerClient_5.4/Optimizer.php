<?php

include_once 'Downloader.php';
include_once 'OptimizerCache.php';

/**
 * Class Optimizer
 */
class Optimizer
{
    /**
     * @var string
     */
    private $optimizerLink = 'http://speed.chernianu.com/api/optimizer';
    /**
     * @var string
     */
    private $userAgentsLink = 'http://speed.chernianu.com/api/userAgent';

    /**
     * @var string
     */
    private $publicKey;

    /**
     * @var string
     */
    private $privateKey;

    /**
     * @var string
     */
    private $optimizerPreKey = 'OPTIMIZER';

    /**
     * @var int
     */
    private $timestamp;

    /**
     * @var Downloader
     */
    private $downloader;

    /**
     * @var OptimizerCache
     */
    private $cache;

    /**
     * Optimizer constructor.
     * @param $publicKey
     * @param $privateKey
     */
    public function __construct($publicKey, $privateKey)
    {
        $this->cache = new OptimizerCache();
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;
    }

    /**
     * @param string $link
     */
    public function getOptimizedPage($link)
    {
        $link = $this->clearLinkParams($link);
        if ($this->isFile($link)) {
            return;
        }

        $file = $this->getFileName($link, 'php');
        $cache = $this->cache->getCache($file);
        if ($cache !== false) {
            echo $cache;
            exit();
        }

        $params = [
            'link' => $link,
            'publicKey' => $this->publicKey,
            'host' => $_SERVER['HTTP_HOST'],
        ];

        $result = $this->sendLink($this->optimizerLink, $params, $file);
        if ($result !== '') {
            echo $result;
            exit();
        }
    }

    /**
     * @param $link
     * @return false|int
     */
    private function isFile($link)
    {
        $pattern = '/\S+\.(svg|jpg|jpeg|js|gif|png|ico|css|bmp|swf|svg|woff|woff2|tar\.gz|html|pdf|doc|docx|xls|xlsx)$/';
        return preg_match($pattern, $link) === 1;
    }

    /**
     * @param string $regexps
     * @param string $userAgent
     * @return bool
     */
    private function checkRegex($regexps, $userAgent)
    {
        $regexps = json_decode($regexps, true);
        if (!is_array($regexps)) {
            return false;
        }

        foreach ($regexps as $regexp) {
            if (preg_match($regexp['regexp'], $userAgent)) {
                return  true;
            }
        }

        return  false;
    }

    /**
     * @param string $userAgent
     * @return bool
     */
    public function checkUserAgent($userAgent)
    {
        if (isset($_GET['optimizer']) && $_GET['optimizer'] === 'optimizer') {
            return false;
        }

        $file = $this->getFileName('user-agent', 'json');
        $cache = $this->cache->getCache($file);
        if ($cache !== false) {
            return $this->checkRegex($cache, $userAgent);
        }

        $params = [
            'publicKey' => $this->publicKey,
            'host' => $_SERVER['HTTP_HOST'],
        ];

        $result = $this->sendLink($this->userAgentsLink, $params, $file);
        return $this->checkRegex($result, $userAgent);
    }

    /**
     * @param string $link
     * @param array $params
     * @param string $file
     * @return string
     */
    private function sendLink($link, array $params, $file)
    {

        $signature = $this->generateSignature($params);
        $authHeader = $this->makeAuthKey($signature);
        $downloader = $this->getDownloader();

        $curlParams = [
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => [
                'Content-Type: ' . $this->getContentType(),
                'Optimizer-Auth: ' . $authHeader,
                'Request-Time: ' . $this->getTimeHeader(),
            ],
        ];

        $downloader->download($link, $curlParams);
        return $this->cache->saveCache($downloader, $file);
    }

    /**
     * @param string $string
     * @param string $ext
     * @return string
     */
    private function getFileName($string, $ext)
    {
        return md5($this->publicKey . $string) . '.' . $ext;
    }

    /**
     * @param string $link
     * @return mixed
     */
    private function clearLinkParams($link)
    {
        $link = explode('?', $link);
        return $link[0];
    }

    /**
     * @param array $params
     * @return string
     */
    private function generateSignature(array $params)
    {
        $method = 'POST';
        ksort($params);
        $md5Params = md5(json_encode($params));

        $contentType = $this->getContentType();
        $stringToSign = implode('+', [
            $method,
            $md5Params,
            $contentType,
            $this->getTimeHeader(),
        ]);

        return base64_encode(hash_hmac(
            'sha256',
            $stringToSign,
            $this->privateKey,
            true
        ));
    }

    /**
     * @return string
     */
    private function getContentType()
    {
        return 'application/json';
    }

    /**
     * @param string $signature
     * @return string
     */
    private function makeAuthKey($signature)
    {
        return $this->optimizerPreKey . ' ' . $this->publicKey . ':' . $signature;
    }

    /**
     * @return Downloader
     */
    private function getDownloader()
    {
        if ($this->downloader === null) {
            $this->downloader = new Downloader();
        }

        return $this->downloader;
    }


    /**
     * @return int
     */
    private function getTimeHeader()
    {
        if ($this->timestamp === null) {
            $this->timestamp = time();
        }

        return $this->timestamp;
    }

    /**
     * @param string $publicKey
     */
    public function clearAllCache($publicKey)
    {
        if ($publicKey === $this->publicKey) {
            $this->cache->clearCache();
        }
    }

}
