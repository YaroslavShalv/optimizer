<?php

include_once 'Optimizer.php';
$optimizer = new Optimizer('{{public}}', '{{private}}');
$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http');
$currentLink = implode('', [
    $protocol,
    '://',
    $_SERVER['HTTP_HOST'],
    $_SERVER['REQUEST_URI'],
]);

if (isset($_GET['optimizerClearCache']) && !empty($_GET['optimizerClearCache'])) {
    $optimizer->clearAllCache($_GET['optimizerClearCache']);
}

if ($optimizer->checkUserAgent($_SERVER['HTTP_USER_AGENT']) || (isset($_GET['test']) && $_GET['test'] == '{{public}}')) {
    $optimizer->getOptimizedPage($currentLink);
}

