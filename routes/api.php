<?php

use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace'     => 'Api',
    'middleware'    => ['api'],
], function (Router $router) {
    $router->post('/optimizer', 'OptimizerController@request');
    $router->post('/userAgent', 'OptimizerController@userAgent');
});
