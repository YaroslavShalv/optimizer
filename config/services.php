<?php

use App\Services\ParseSite\Collectors\CSSCollector;
use App\Services\ParseSite\Collectors\ImagesCollector;
use App\Services\ParseSite\Collectors\JSCollector;
use App\Services\ParseSite\Collectors\HtmlOnlyCollector;
use App\Services\ParseSite\Collectors\NodeCollector;
use App\Services\ParseSite\Collectors\ScreenShotCollector;

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    //Order is important!
    'configuratorService' => [
        'deleteAllBesidesHtml' => [
            'class' => HtmlOnlyCollector::class,
            'params' => [],
        ],
        'optimizeCss' => [
            'class' => CSSCollector::class,
            'params' => [
                'cutFonts' => 0,
            ],
        ],
        'optimizeJs' => [
            'class' => JSCollector::class,
            'params' => [],
        ],
        'imaToBase64' => [
            'class' => ImagesCollector::class,
            'params' => [
                'cssImagesToBase64' => 0,
            ],
        ],
        'makeSiteScreenshot' => [
            'class' => ScreenShotCollector::class,
            'params' => [
                'serviceUrl' => 'https://www.site-shot.com/screenshot/',
                'desktop' => [
                    'width' => '1000',
                    'height' => '696',
                    'zoom' => 100,
                    'scaled_width' => 500,
                    'format' => 'jpeg',
                    'user_agent' => '',
                    'delay_time' => '500',
                    'url' => '',
                ],
                'mobile' => [
                    'width' => '312',
                    'height' => '500',
                    'zoom' => 100,
                    'scaled_width' => 312,
                    'format' => 'jpeg',
                    'user_agent' => '',
                    'delay_time' => '500',
                    'url' => '',
                ],
            ],
        ],
        'nodeOptimization' => [
            'class' => NodeCollector::class,
            'params' => [
                'serviceUrl' => 'http://80.78.254.115/screen/green',
            ],
        ],
    ],
];
