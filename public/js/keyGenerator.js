(function () {
    $("#generatePublic, #generatePrivate").on('click', function (ecdhKeyDeriveParams) {
        generateCode(this);
    })

    function generateCode(element) {
        let csrf = $('meta[name="csrf-token"]').attr('content');
        let url = $(element).data('url');
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            beforeSend: function(request) {
                request.setRequestHeader("X-CSRF-TOKEN", csrf);
            },
            success: function(result) {
                if (result && result.key) {
                    $(element).parent().parent().find('input').val(result.key);
                }
            }
        });
    }
})();
