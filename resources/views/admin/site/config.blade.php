<?php
/**
 * @var \App\Models\Configurator[] $defaultConfig
 * @var \App\Models\ConfiguratorsSite[] $siteConfigs
 */
?>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Site config</h3>
    </div>

    <form action="" method="post" accept-charset="UTF-8" class="form-horizontal">
        {{ csrf_field() }}
        <div class="box-body">
            <div class="fields-group">
                <div class="col-md-12">
                    <?php foreach ($defaultConfig as $configurator) : ?>
                        <?php
                            $value = (isset($siteConfigs[$configurator->id]))
                                ? $siteConfigs[$configurator->id]->value
                                : $configurator->defaultValue;
                            $checkbox = (bool) $value === true ? "checked='checked'" : "";
                        ?>
                        <div class="form-group">
                            <label for="status" class="col-sm-6  control-label"><?= $configurator->name ?></label>
                            <div class="col-sm-6">
                                <label style="margin: 8px 10px 0 0;">
                                    <input type='hidden' value='0' name='configurator[<?= $configurator->id ?>]'>
                                    <input type='checkbox' value='1' name="configurator[<?= $configurator->id ?>]" <?= $checkbox ?>>
                                </label>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>


            <div class="box-footer">
                <div class="col-md-8">
                    <div class="btn-group pull-right">
                        <input type="submit" value="Submit" \>
                    </div>
                </div>
            </div>
        </div>

    </form>
            </div>
        </div>
    </div>
</section>
