<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAgentTable extends Migration
{
    /**
     * @var string
     */
    private $table = 'user_agents';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('Name');
            $table->string('regexp')->comment('Regular exception');
            $table->smallInteger('status')->comment('Enabled/Disabled')->default(1);
            $table->string('comment')->comment('Short comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
