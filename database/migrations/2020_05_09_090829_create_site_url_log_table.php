<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteUrlLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_url_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('site_url_id')->unsigned();
            $table->text('parse_log');
            $table->tinyInteger('status');
            $table->timestamps();
            $table->foreign('site_url_id')->references('id')->on('site_urls')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_url_logs');
    }
}
