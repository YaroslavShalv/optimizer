<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfiguratorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('configurators')) {
            return;
        }

        Schema::create('configurators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('type');
            $table->string('key');
            $table->integer('status');
            $table->string('defaultValue');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configurators');
    }
}
