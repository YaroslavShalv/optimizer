<?php

use App\Models\Configurator;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddNewCollector extends Migration
{
    private $configurations = [
        [
            'type' => 1,
            'key' => 'optimizeJs',
            'status' => 1,
            'defaultValue' => '0',
            'name' => 'Объединить и сжать JS',
        ],
        [
            'type' => 1,
            'key' => 'optimizeCss',
            'status' => 1,
            'defaultValue' => '0',
            'name' => 'Обьединить и оптимизировать CSS',
        ],
        [
            'type' => 1,
            'key' => 'imaToBase64',
            'status' => 1,
            'defaultValue' => '0',
            'name' => 'Картинки в тега img преобразовать в base64',
        ],
        [
            'type' => 1,
            'key' => 'cutFonts',
            'status' => 1,
            'defaultValue' => '0',
            'name' => 'Вырезать шрифты',
        ],
        [
            'type' => 1,
            'key' => 'cssImagesToBase64',
            'status' => 1,
            'defaultValue' => '0',
            'name' => 'Картинки из css в base64',
        ],
        [
            'type' => 1,
            'key' => 'deleteAllBesidesHtml',
            'status' => 1,
            'defaultValue' => '1',
            'name' => 'Удалить всё кроме html',
        ],
        [
            'type' => 1,
            'key' => 'makeSiteScreenshot',
            'status' => 1,
            'defaultValue' => '1',
            'name' => 'Создать скриншот сайта',
        ],
        [
            'type' => 1,
            'key' => 'nodeOptimization',
            'status' => 1,
            'defaultValue' => 0,
            'name' => 'Оптимизация на стороне nodeJs',
        ],
    ];


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $existsConfiguration = Configurator::all();
        $existKeys = [];
        foreach ($existsConfiguration as $configurator) {
            $existKeys[] = $configurator->key;
        }

        $toInsert = [];
        foreach ($this->configurations as $configuration) {
            if (in_array($configuration['key'], $existKeys)) {
                continue;
            }
            $configuration['created_at'] = date('Y-m-d H:i:s');
            $configuration['updated_at'] = date('Y-m-d H:i:s');
            $toInsert[] = $configuration;
        }

        if (count($toInsert) == 0) {
            return;
        }

        DB::table('configurators')->insert($toInsert);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
